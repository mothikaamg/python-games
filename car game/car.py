import pygame

pygame.init()

width = 960
height = 540


clock = pygame.time.Clock()
screen = pygame.display.set_mode((width, height))


bg = pygame.image.load("SampleEnvironmentImage.png")
image = pygame.image.load("SampleCharacterImage.png")
image = pygame.transform.scale(image,(300,350))

x = 0
y = 250


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x-=50
            elif event.key == pygame.K_RIGHT:
                x += 50

        screen.blit(bg,(0,0))
        screen.blit(image,(x,y))

        pygame.display.flip()
        clock.tick(60)





