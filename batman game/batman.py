import turtle
import time
import random

is_start = True
is_jumping = False

start_button = turtle.Turtle()
start_screen = turtle.Turtle()

screen = turtle.Screen()
screen.setup(960, 540)

positionX = -200
positionY = -160

batman_health = 200
darkseid_health = 200

batman = turtle.Turtle()
darkseid = turtle.Turtle()

darkseid_health_turtle = turtle.Turtle()
batman_health_turtle = turtle.Turtle()

batman_head = turtle.Turtle()
darkseid_head = turtle.Turtle()

game_over = turtle.Turtle()
you_win = turtle.Turtle()

game_over.hideturtle()
you_win.hideturtle()

move_speed = 25

screen.addshape("start_screen_frame_1.gif")
screen.addshape("start_screen_frame_2.gif")
screen.addshape("start_screen_frame_3.gif")
screen.addshape("start_screen_frame_4.gif")
screen.addshape("start_button_1.gif")
screen.addshape("start_button_2.gif")
screen.addshape("storyboard_1.gif")
screen.addshape("storyboard_2.gif")
screen.addshape("storyboard_3.gif")
screen.addshape("storyboard_4.gif")
screen.addshape("storyboard_5.gif")
screen.addshape("storyboard_6.gif")
screen.addshape("storyboard_7.gif")
screen.addshape("storyboard_8.gif")
screen.addshape("storyboard_9.gif")
screen.addshape("storyboard_10.gif")
screen.addshape("storyboard_11.gif")
screen.addshape("storyboard_12.gif")
screen.addshape("storyboard_13.gif")
screen.addshape("storyboard_14.gif")
screen.addshape("storyboard_15.gif")
screen.addshape("storyboard_16.gif")
screen.addshape("storyboard_17.gif")
screen.addshape("storyboard_18.gif")
screen.addshape("storyboard_19.gif")
screen.addshape("storyboard_20.gif")
screen.addshape("storyboard_21.gif")
screen.addshape("storyboard_22.gif")
screen.addshape("storyboard_23.gif")
screen.addshape("storyboard_24.gif")
screen.addshape("storyboard_25.gif")
screen.addshape("storyboard_26.gif")
screen.addshape("storyboard_27.gif")
screen.addshape("storyboard_28.gif")
screen.addshape("storyboard_29.gif")
screen.addshape("storyboard_30.gif")
screen.addshape("storyboard_31.gif")
screen.addshape("storyboard_32.gif")
screen.addshape("storyboard_33.gif")
screen.addshape("storyboard_34.gif")
screen.addshape("storyboard_35.gif")
screen.addshape("storyboard_36.gif")
screen.addshape("storyboard_37.gif")
screen.addshape("storyboard_38.gif")
screen.addshape("storyboard_39.gif")
screen.addshape("storyboard_40.gif")
screen.addshape("storyboard_41.gif")
screen.addshape("storyboard_42.gif")
screen.addshape("storyboard_43.gif")
screen.addshape("storyboard_44.gif")
screen.addshape("storyboard_45.gif")
screen.addshape("storyboard_46.gif")
screen.addshape("storyboard_47.gif")
screen.addshape("gothamcity.gif")
screen.addshape("batman_1.gif")
screen.addshape("batman_head.gif")
screen.addshape("darkseid_head.gif")
screen.addshape("darkseid_1.gif")
screen.addshape("batman_jump.gif")
screen.addshape("batman_punch.gif")
screen.addshape("batman_kick_1.gif")
screen.addshape("batman_kick_2.gif")
screen.addshape("batman_kick_3.gif")
screen.addshape("darkseid_kick.gif")
screen.addshape("darkseid_punch.gif")
screen.addshape("you_win.gif")
screen.addshape("game_over.gif")




def load_start_screen():
    for i in range(3):
        start_screen.shape("start_screen_frame_1.gif")
        time.sleep(0.05)
        start_screen.shape("start_screen_frame_2.gif")
        time.sleep(0.05)
        start_screen.shape("start_screen_frame_3.gif")
        time.sleep(0.05)
        start_screen.shape("start_screen_frame_4.gif")
        time.sleep(0.05)
        start_screen.shape("start_screen_frame_3.gif")
        time.sleep(0.05)
        start_screen.shape("start_screen_frame_2.gif")
        time.sleep(0.05)
        start_screen.shape("start_screen_frame_1.gif")
        time.sleep(0.05)

    while is_start:
        show_start_button()

def show_start_button():
    global start_button
    turtle.onscreenclick(load_intro_animation)

    for i in range(2):
        start_button.shape("start_button_1.gif")
        time.sleep(0.5)
        start_button.shape("start_button_2.gif")
        time.sleep(0.1)
        start_button.shape("start_button_1.gif")
        time.sleep(0.1)
        start_button.shape("start_button_2.gif")
        time.sleep(0.1)

def load_intro_animation(x, y):
    global start_button
    global start_screen
    global is_start

    is_start = False
    turtle.onscreenclick(None)
    start_button.hideturtle()
    start_screen.hideturtle()
    intro = turtle.Turtle()
    intro.shape("storyboard_1.gif")
    time.sleep(4)
    intro.shape("storyboard_2.gif")
    time.sleep(4)
    intro.shape("storyboard_3.gif")
    time.sleep(4)
    intro.shape("storyboard_4.gif")
    time.sleep(4)
    intro.shape("storyboard_5.gif")
    time.sleep(2)
    intro.shape("storyboard_6.gif")
    time.sleep(0.4)
    intro.shape("storyboard_7.gif")
    time.sleep(0.4)
    intro.shape("storyboard_8.gif")
    time.sleep(0.4)
    intro.shape("storyboard_9.gif")
    time.sleep(0.4)
    intro.shape("storyboard_10.gif")
    time.sleep(0.4)
    intro.shape("storyboard_11.gif")
    time.sleep(0.4)
    intro.shape("storyboard_12.gif")
    time.sleep(0.4)
    intro.shape("storyboard_13.gif")
    time.sleep(0.4)
    intro.shape("storyboard_14.gif")
    time.sleep(0.4)
    intro.shape("storyboard_15.gif")
    time.sleep(0.4)
    intro.shape("storyboard_16.gif")
    time.sleep(0.4)
    intro.shape("storyboard_17.gif")
    time.sleep(0.4)
    intro.shape("storyboard_18.gif")
    time.sleep(0.4)
    intro.shape("storyboard_19.gif")
    time.sleep(0.4)
    intro.shape("storyboard_20.gif")
    time.sleep(0.4)
    intro.shape("storyboard_21.gif")
    time.sleep(0.4)
    intro.shape("storyboard_22.gif")
    time.sleep(0.4)
    intro.shape("storyboard_23.gif")
    time.sleep(0.4)
    intro.shape("storyboard_24.gif")
    time.sleep(0.4)
    intro.shape("storyboard_25.gif")
    time.sleep(0.4)
    intro.shape("storyboard_26.gif")
    time.sleep(0.4)
    intro.shape("storyboard_27.gif")
    time.sleep(0.4)
    intro.shape("storyboard_28.gif")
    time.sleep(0.4)
    intro.shape("storyboard_29.gif")
    time.sleep(0.4)
    intro.shape("storyboard_30.gif")
    time.sleep(0.4)
    intro.shape("storyboard_31.gif")
    time.sleep(0.4)
    intro.shape("storyboard_32.gif")
    time.sleep(0.4)
    intro.shape("storyboard_33.gif")
    time.sleep(0.4)
    intro.shape("storyboard_34.gif")
    time.sleep(0.4)
    intro.shape("storyboard_35.gif")
    time.sleep(0.4)
    intro.shape("storyboard_36.gif")
    time.sleep(0.4)
    intro.shape("storyboard_37.gif")
    time.sleep(0.4)
    intro.shape("storyboard_38.gif")
    time.sleep(0.4)
    intro.shape("storyboard_39.gif")
    time.sleep(0.4)
    intro.shape("storyboard_40.gif")
    time.sleep(0.4)
    intro.shape("storyboard_41.gif")
    time.sleep(0.4)
    intro.shape("storyboard_42.gif")
    time.sleep(0.4)
    intro.shape("storyboard_43.gif")
    time.sleep(0.4)
    intro.shape("storyboard_44.gif")
    time.sleep(0.4)
    intro.shape("storyboard_45.gif")
    time.sleep(0.4)
    intro.shape("storyboard_46.gif")
    time.sleep(0.4)
    intro.shape("storyboard_47.gif")
    time.sleep(2)
    intro.hideturtle()
    load_game()

def load_game():
    global start_button
    global batman
    global darkseid
    global batman_health_turtle
    global darkseid_health_turtle

    bg = turtle.Turtle()
    bg.shape("gothamcity.gif")

    batman_health_turtle.shape("square")
    batman_health_turtle.up()
    batman_health_turtle.color("lime")
    batman_health_turtle.goto(-320, 155)
    batman_health_turtle.width(25)
    batman_health_turtle.hideturtle()
    batman_health_turtle.down()
    batman_health_turtle.forward(batman_health)
    
    batman_head.up()
    batman_head.shape("batman_head.gif")
    batman_head.goto(-400, 200)
    
    darkseid_health_turtle.shape("square")
    darkseid_health_turtle.up()
    darkseid_health_turtle.color("lime")
    darkseid_health_turtle.goto(320, 155)
    darkseid_health_turtle.width(25)
    darkseid_health_turtle.hideturtle()
    darkseid_health_turtle.down()
    

    darkseid_health_turtle.back(darkseid_health)

    darkseid_head.up()
    darkseid_head.shape("darkseid_head.gif")
    darkseid_head.goto(400, 200)

    darkseid.shape("darkseid_1.gif")
    batman.shape("batman_1.gif")

    batman.penup()
    batman.speed(0)
    batman.goto(-200, -160)

    darkseid.penup()
    darkseid.speed(0)
    darkseid.goto(200, -160)

    time.sleep(2)

    while batman_health > 0 and darkseid_health > 0:
        if batman.xcor() < darkseid.xcor() and darkseid.xcor() - batman.xcor() > 200:
            darkseid.backward(100)
        elif batman.xcor() > darkseid.xcor():
            darkseid.forward(100)
        time.sleep(1)
        number = random.randrange(0,100)

        if number > 50:
            darkseid.shape("darkseid_punch.gif")
            if is_collided_with(darkseid, batman):
                draw_health("batman", batman_health_turtle, batman_health, 25)
            time.sleep(0.15)
            darkseid.shape("darkseid_1.gif")
        else:
            darkseid.shape("darkseid_kick.gif")
            if is_collided_with(darkseid, batman):
                draw_health("batman", batman_health_turtle, batman_health, 25)
            time.sleep(0.15)
            darkseid.shape("darkseid_1.gif") 
        time.sleep(1)  


def right():
    global batman
    batman.forward(move_speed) 

def left():
    global batman
    batman.backward(move_speed)

def punch():
    global batman
    batman.shape("batman_punch.gif")
    if is_collided_with(batman, darkseid):
        draw_health("darkseid", darkseid_health_turtle, darkseid_health, 25 )
    turtle.ontimer(lambda:batman.shape("batman_1.gif"), 150)

def kick():
    global batman
    batman.shape("batman_kick_1.gif")
    time.sleep(0.1)
    batman.shape("batman_kick_2.gif")
    time.sleep(0.1)
    batman.shape("batman_kick_3.gif")
    time.sleep(0.1)
    if is_collided_with(batman, darkseid):
        draw_health("darkseid", darkseid_health_turtle, darkseid_health, 25 )
    turtle.ontimer(lambda:batman.shape("batman_1.gif"), 150)

def jump():
    global positionX
    global positionY
    global is_jumping
    global batman
    if is_jumping:
        return 
    is_jumping = True
    positionX = batman.xcor()
    positionY = batman.ycor()
    batman.hideturtle()
    batman.left(90)
    batman.forward(100)
    batman.shape("batman_jump.gif")
    batman.showturtle()
    turtle.ontimer(idle, 150)

def idle():
    global is_jumping
    global batman
    batman.hideturtle()
    batman.right(90)
    batman.forward(100)
    batman.shape("batman_1.gif")
    batman.goto(positionX,positionY)
    batman.showturtle()
    is_jumping = False

def is_collided_with(a, b):
    return abs(a.xcor() - b.xcor()) < 250 or a.xcor() - b.xcor() > -250 and abs(a.ycor() - b.ycor()) < 200 

def draw_health(character, health_turtle, health_amount, damage):   
    global batman_health
    global darkseid_health
    health_turtle.color("black")

    if health_amount < 0:
        health_amount = 0

    if character == "batman":
        health_turtle.back(health_amount)
        health_amount = health_amount - damage
        batman_health = health_amount
    else:
        health_turtle.forward(health_amount)
        health_amount = health_amount - damage
        darkseid_health = health_amount      

    if health_amount <= 0:

        if character == "batman":
            game_over.shape("game_over.gif")
            game_over.showturtle()
        else:
            you_win.shape("you_win.gif")
            you_win.showturtle()
        return 
    elif health_amount <= 50:
        health_turtle.color("red")
    elif health_amount <= 100:
        health_turtle.color("yellow")
    else:
        health_turtle.color("lime")

    if character == "batman":
        health_turtle.forward(health_amount)
    else:
        health_turtle.back(health_amount) 












screen.onkey(left, "Left")
screen.onkey(right, "Right")
screen.onkey(punch, "z")
screen.onkey(kick, "x")
screen.onkey(jump, "s")







screen.listen()
load_start_screen()
screen.mainloop()